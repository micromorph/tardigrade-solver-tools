.. _changelog:


#########
Changelog
#########


******************
0.3.1 (unreleased)
******************

******************
0.3.0 (09-01-2022)
******************

Release
=======
- Released version 0.3.0 (:merge:`12`)

Internal Changes
================
- Build, package, and deploy as a Conda package to the AEA Conda channel (:merge:`9`). By `Nathan Miller`_.
- Added the changelog (:merge:`9`). By `Nathan Miller`_.
- Added the updated environment definition (:merge:`10`). By `Nathan Miller`_.
- Added the updated gitlab-ci.yaml file (:merge:`11`). By `Nathan Miller`_. and `Kyle Brindley`_.
