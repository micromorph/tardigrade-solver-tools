workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: always

stages:
  - environment
  - version
  - test
  - build
  - deploy

before_script:
  # TODO: recover environment path from modulefile instead of assuming the sstelmo path(s)
  # https://re-git.lanl.gov/aea/python-projects/waves/-/issues/7
  - aea_projects='/projects'
  - aea_deploy_directory="${aea_projects}/aea_compute"
  - aea_conda_channel="${aea_deploy_directory}/aea-conda"
  - aea_modulefiles=${aea_deploy_directory}/modulefiles
  - environment='solver_tools-env'
  - environment_path="${aea_deploy_directory}/${environment}"
  - conda_options="--yes --channel ${aea_conda_channel} --channel conda-forge"
  # Activate the project specific environment from the project specific modulefile
  - module use ${PWD}/modulefiles
  - module load ${environment}
  # Build the Conda environment if it's missing
  # TODO: kick off 'environment' job for missing environments instead of re-creating the environment build logic here
  # https://re-git.lanl.gov/kbrindley/waves/-/issues/8
  - |
      if [[ ! -d ${environment_path} ]]; then
          export ALL_PROXY="proxyout.lanl.gov:8080"
          export HTTP_PROXY="http://$ALL_PROXY"
          export HTTPS_PROXY=$HTTP_PROXY
          conda create --prefix ${environment_path} --file environment.txt ${conda_options};
          unset ALL_PROXY
          unset HTTP_PROXY
          unset HTTPS_PROXY
      fi
  # FIXME: (1) Without setting this to false, Git webserver API calls to re-git.lanl.gov will throw errors about
  # self-signed certificates. Work on CI server and Gitlab webserver configurations so that this is no longer
  # necessary. There is a matching "FIXME: (1)" tag where the process is reversed that must also be removed when this
  # is fixed.
  - git config --local http.sslVerify false

after_script:
  # FIXME: (1) Reset the repository Git configuration to preserve ssl verifications. Remove when the server(s)
  # configurations no longer require us to drop ssl verifications.
  - git config --local http.sslVerify true

environment:
  stage: environment
  variables:
    GIT_STRATEGY: clone
  script:
    - export ALL_PROXY="proxyout.lanl.gov:8080"
    - export HTTP_PROXY="http://$ALL_PROXY"
    - export HTTPS_PROXY=$HTTP_PROXY
    # Re-build the Conda environment on changes to environment files
    - conda create --prefix ${environment_path} --file environment.txt ${conda_options}
    # Remove write permissions from group to avoid accidental environment changes
    - chmod -R 755 ${environment_path}
    # place the common modulefiles in an accessible location
    - cp ${PWD}/modulefiles/* ${aea_modulefiles}
  only:
    refs:
      - main
      - dev
    changes:
      - "modulefiles/*"
      - "environment.txt"
  tags:
    - sstelmo-shell-aea

microbump:
  stage: version
  variables:
    GIT_STRATEGY: clone
  script:
    # Conditionally "bump" micro version number. setuptools_scm already bumps number, just need to strip local version.
    - old_version=$(python -m setuptools_scm)
    # First capture group is the major.minor.micro numbers
    # Second capture group is everything following micro
    - version_regex='\([0-9]\+\.[0-9]\+\.[0-9]\+\)\(.*\)'
    # Returns clean production tag regardless if tagged already
    - production_version=$(echo ${old_version} | sed "s/${version_regex}/\1/g")
    # Catch unexpected production version regex and exit with error if suffix is found
    - suffix=$(echo ${production_version} | sed "s/${version_regex}/\2/g")
    - |
        if [ -n "${suffix}" ]; then
            echo "Could not resolve the production version from ${old_version}. Left with ${production_version} and ${suffix}."
            exit 1
        fi
    - developer_version=${production_version}+dev
    # Tag production commit and previous developer commit. Continue if already tagged.
    - git config user.name "${GITLAB_USER_NAME}"
    - git config user.email "${GITLAB_USER_EMAIL}"
    - git remote add oauth2-origin https://gitlab-ci-token:${GITLAB_ACCESS_TOKEN}@re-git.lanl.gov/${CI_PROJECT_PATH}.git
    - git tag -a ${production_version} -m "production release ${production_version}" || true
    # Assume last merge was dev->main. Pick previous.
    - last_merge_hash=$(git log --pretty=format:"%H" --merges -n 2 | tail -n 1)
    - git tag -a ${developer_version} -m "developer pre-release ${developer_version}" ${last_merge_hash} || true
    - git push oauth2-origin --tags
  tags:
    - sstelmo-shell-aea
  only:
    - main

fast-test:
  stage: test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  variables:
    GIT_STRATEGY: clone
  script:
    - workdir=${PWD}
    - mkdir build
    - cd build
    - cmake .. -DCMAKE_BUILD_TYPE=Release
    - cmake --build . --verbose
    - ctest --verbose --output-log results.txt
    - cd ${workdir}
  artifacts:
    when: always
    paths:
      - build/results.txt
  tags:
    - sstelmo-shell-aea

conda-build:
  stage: build
  variables:
    GIT_STRATEGY: clone
  script:
    # Set the LANL internal proxies
    - export ALL_PROXY="proxyout.lanl.gov:8080"
    - export HTTP_PROXY="http://$ALL_PROXY"
    - export HTTPS_PROXY=$HTTP_PROXY
    # Override default permissions. Set group to rx with no write permissions.
    - umask 0022
    - output_folder='conda-build-artifacts'
    - mkdir ${output_folder}
    - VERSION=$(python -m setuptools_scm) conda build recipe --channel file://${aea_conda_channel} --channel conda-forge --no-anaconda-upload --croot /scratch/${USER}/conda-build --output-folder ${output_folder}
  artifacts:
    expire_in: '2 hrs'
    paths:
      - conda-build-artifacts/linux-64/solver_tools-*-*.tar.bz2
  tags:
    - sstelmo-shell-aea
  only:
    - merge_requests
    - main
    - dev

deploy:
  stage: deploy
  variables:
    GIT_STRATEGY: clone
  script:
    # Override default permissions. Set group to rx with no write permissions.
    - umask 0022
    # Copy Conda package to AEA Conda Channel
    - cp conda-build-artifacts/linux-64/solver_tools-*-*.tar.bz2 ${aea_conda_channel}/linux-64
    # Change group for access by all W-13 staff and prevent accidental modification by institutional account in CI jobs
    - chgrp w13users ${aea_conda_channel}/linux-64/solver_tools-*-*.tar.bz2 || true
    - chmod 555 ${aea_conda_channel}/linux-64/solver_tools-*-*.tar.bz2 || true
    # Update the AEA Conda Channel index
    - conda index ${aea_conda_channel}
    # Troubleshooting conda channel deploy and index update
    - conda search --channel file://${aea_conda_channel}/ --override-channels solver_tools
  tags:
    - sstelmo-shell-aea
  only:
    - main
    - dev

# It MUST be called pages
pages:
  stage: deploy
  variables:
    GIT_STRATEGY: clone
  script:
    - workdir=${PWD}
    - mkdir -p public/doxygen
    - mkdir build
    - cd build
    - cmake ..
    - cmake --build . --target Sphinx
    - cd ${workdir}
    - cp -r build/docs/sphinx/html/* public/
    - cp -r build/docs/doxygen/html/* public/doxygen
  artifacts:
    paths:
      # It MUST be called public
      - public
  tags:
    - sstelmo-shell-aea
  only:
    - main

